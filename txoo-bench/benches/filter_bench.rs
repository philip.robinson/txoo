use bitcoin::{Transaction, PackedLockTime, OutPoint, TxIn, Txid, TxOut, hashes::Hash, Block, BlockHeader, BlockHash, TxMerkleNode};
use criterion::{Criterion, criterion_main, criterion_group, black_box};
use txoo::filter::BlockSpendFilter;

fn filter_test(c: &mut Criterion) {
  let mut txs = Vec::new();
  for i in 0..16665 {
      let tx = Transaction {
          version: 0,
          lock_time: PackedLockTime(0),
          input: vec![TxIn {
              previous_output: OutPoint {
                  txid: Txid::all_zeros(),
                  vout: i,
              },
              script_sig: Default::default(),
              sequence: Default::default(),
              witness: Default::default(),
          }],
          output: vec![TxOut {
              value: 0,
              script_pubkey: Default::default(),
          }],
      };
      txs.push(tx);
  }
  let block = Block {
      header: BlockHeader {
          version: 0,
          prev_blockhash: BlockHash::all_zeros(),
          merkle_root: TxMerkleNode::all_zeros(),
          time: 0,
          bits: 0,
          nonce: 0,
      },
      txdata: txs,
  };
  let block_hash = block.block_hash();
  let mut filter: BlockSpendFilter = BlockSpendFilter::from_block(&block);

  let mut group = c.benchmark_group("compact filter");
  group.bench_function("generation", |b| {
    b.iter(|| {
      filter = BlockSpendFilter::from_block(black_box(&block));
    })
  });

  group.bench_function("consumption match any", |b| {
    b.iter(|| filter.match_any(
      &block_hash,
      &mut vec![
          OutPoint {
              txid: Txid::all_zeros(),
              vout: 1234,
          },
          OutPoint {
              txid: Txid::all_zeros(),
              vout: 5555,
          }
      ]
      .iter()
    ))
  });

  group.finish();
}

criterion_group!(benches, filter_test);
criterion_main!(benches);
