use crate::{Attestation, SignedAttestation};
use bitcoin::hashes::Hash;
use bitcoin::secp256k1::{All, Message, Secp256k1};
use bitcoin::key::KeyPair;

/// Sign an attestation
pub fn sign_attestation(
    attestation: Attestation,
    keypair: &KeyPair,
    secp: &Secp256k1<All>,
) -> SignedAttestation {
    let message = Message::from_slice(&attestation.hash().to_byte_array()).expect("sign message");
    let signature = secp.sign_schnorr_no_aux_rand(&message, &keypair);
    SignedAttestation {
        attestation,
        signature,
    }
}
