#[cfg(feature = "system-test")]
#[tokio::test]
async fn bitcoind_system_test() {
    let client =
        bitcoind_client::BitcoindClient::new("http://user:pass@localhost:18443".parse().unwrap())
            .await;
    let info = client.get_blockchain_info().await.unwrap();
    println!("info: {:?}", info);
}

#[cfg(feature = "external-esplora-test")]
mod external_esplora_tests {
    use std::str::FromStr;
    use bitcoin::hashes::Hash;
    use bitcoin::OutPoint;
    use bitcoin::Txid;
    use bitcoind_client::{esplora_client, Explorer};

    #[tokio::test]
    async fn utxo_test() {
        let client =
            esplora_client::EsploraClient::new("https://blockstream.info/api".parse().unwrap())
                .await;
        let txo1 = OutPoint::new(
            Txid::from_str("e9a66845e05d5abc0ad04ec80f774a7e585c6e8db975962d069a522137b80c1d")
                .unwrap(),
            0,
        );
        let res = client.get_utxo_confirmations(&txo1).await.unwrap();
        assert!(res.is_none());
        let res = client.get_utxo_spending_tx(&txo1).await.unwrap();
        assert_eq!(
            res.expect("expected spent tx").txid().to_string(),
            "1ebf2dd645b815a59e35485dd4ceebc0587a1b7588b810e42085b479ce507bfa"
        );
    }

    #[tokio::test]
    async fn dummy_test() {
        let client =
            esplora_client::EsploraClient::new("https://blockstream.info/api".parse().unwrap())
                .await;
        let dummy_txo = OutPoint::new(Txid::all_zeros(), 0);
        let res = client.get_utxo_confirmations(&dummy_txo).await.unwrap();
        assert!(res.is_none());
    }
}
